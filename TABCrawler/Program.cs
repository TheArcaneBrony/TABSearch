﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;
using HtmlAgilityPack;
using TABSiteMapper;

namespace TABCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            StartProcessSite("https://thearcanebrony.net");
        }
        
        static WebClient wc = new WebClient();
        static string domain = "";
        private static void StartProcessSite(string url)
        {
            domain = url;
            new Thread(() => ProcessSite(url)).Start();
        }
        private static void ProcessSite(string url)
        {
            
            if (domain == "") domain = url;
            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = hw.Load(url);

            if (doc.DocumentNode.SelectNodes("//*[@href]") != null) foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//*[@href]")) ProcessNode(link, url);
            if (doc.DocumentNode.SelectNodes("//iframe") != null) foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//iframe")) ProcessNode(link, url, "src");
        }
        private static List<string> foundUrls = new();
        private static void ProcessNode(HtmlNode link, string url, string urlprop = "href")
        {
            string nurl = link.Attributes[urlprop].Value, neurl = UrlUtil.GetFullUrl(domain, url, nurl).Replace("//", "/").Replace("http:/", "http://").Replace("https:/", "https://");
            Debug.WriteLine($"FOUND URL: {nurl} at {url}\nEvaluating to {neurl}");
            if (neurl.StartsWith(domain) && !foundUrls.Contains(neurl.Replace("/", "").ToLower()) && !(
                neurl.EndsWith(".css") || neurl.EndsWith(".js") || neurl.EndsWith(".osr") || neurl.EndsWith(".json") || neurl.EndsWith(".xml")
                || neurl.EndsWith(".png") || neurl.Contains("./")
            ))
            {

                foundUrls.Add(neurl.Replace("/", "").ToLower());
                Console.WriteLine(neurl);
                Thread.Sleep(10);
                ProcessSite(neurl);
            }
        }
    }
}