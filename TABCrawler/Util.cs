﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TABSiteMapper
{
    class Util
    {
        public static void Log(string text)
        {
            Console.WriteLine(text);
        }
    }
    class UrlUtil
    {
        public static int GetUrlDepth(string url)
        {
            return Regex.Matches(url.TrimEnd('/'), "/").Count - 2;
        }
        public static string GetFullUrl(string domain, string currentUrl, string url)
        {
            if (url.StartsWith("/")) return $"{domain}/{url}";
            else if (url.StartsWith("https://") || url.StartsWith("http://")) return url;
            else
            {
                List<string> parts = currentUrl.Split('/').ToList();
                if (currentUrl.EndsWith(".html") || currentUrl.EndsWith(".php")) parts.RemoveAt(parts.Count - 1);
                return $"{string.Join('/', parts)}/{url}";
            }
            return "";
        }
    }
    static class Extensions
    {
        public static bool ContainsAnyCase(this string[] items, string text)
        {
            foreach (string item in items) if (((string)item).ToLower() == text.ToLower()) return true;
            return false;
        }
    }
}
