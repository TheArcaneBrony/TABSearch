﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TABSearch.Controllers
{
        [Controller]
        [Route("/")]
        public class HealthController : ControllerBase
        {

            [HttpGet]
            public async Task<ContentResult> Get()
            {
               
                return new ContentResult { Content = @"
<!DOCTYPE html>
<html lang='en'>
<head>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta name='Description' content='TABSearch'>
	<title>TABSearch</title>
	<meta property='og:title' content='TABSearch' />
	<meta property='og:url' content='https://search.thearcanebrony.net/' />
	<meta property='og:image' content='https://search.thearcanebrony.net/logo.png' />
	<link rel='stylesheet' href='/style.css'>
	<script src='https://code.jquery.com/jquery-3.6.0.min.js'></script>
	<iframe id='navbar' src='/navbar.html' title='Navigation bar'></iframe>
</head>
<main>
	<div id='body'>
<center>
	<img src='logo.png' style='border-radius: 50%; width:256px' alt='TABSearch logo'/><br><br>
	<input id='q'/>
	<button id='s'>Search</button>
	<script>$('#s').click(x=>{document.location.href = document.location.origin +'/search?q='+$('#q')[0].value})</script>
</center>
</div>
<iframe id='footer' src='/footer.html' title='Footer'/>
</main>
</html>", ContentType = "text/html"};
            }
        }
}