﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TABSearch.Controllers
{
        [Controller]
        [Route("/")]
        public class StylesheetController : ControllerBase
        {

            [HttpGet("/style.css")]
            public async Task<ContentResult> Get()
            {
               
                return new ContentResult { Content = @"
#navbar{
  width: 100%;
  border: none;
  height:55px;
}
#body {
  margin-top: 10px;
}
body{
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  background-color: #232222;
  color: white;
  margin: 0px;
}
a{
  color: white;
}

  
  name { font-size:20; vertical-align: middle;}
  user { width:500px; display: inline-block; background-color: #a2a2a20f; border-radius:25px; padding: 20px; margin: 5px; }
  #avatar { height:64px; width: 64px; border-radius: 50%; vertical-align: middle; }


  #footer {
    position: fixed;
    bottom: 0;
    vertical-align: bottom;
    width: 100%;
    height: 32px;
    border: none;
  }
", ContentType = "text/css"};
            }
        }
}