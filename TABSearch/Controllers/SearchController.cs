﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TABSearch.Controllers
{
        [Controller]
        [Route("/")]
        public class SearchController : ControllerBase
        {

            [HttpGet("/search")]
            public async Task<ContentResult> Get()
            {
	            string q = Request.Query["q"];
	            string[] results = (await System.IO.File.ReadAllLinesAsync("db.txt")).Where(x=>x.Contains(q)).ToArray();
	            string resultsHtml = "";
	            foreach (var res in results)
	            {
		            resultsHtml += $"<a href='{res}'>{res}</a><br>";
	            }
	            return new ContentResult { Content = @"
<!DOCTYPE html>
<html lang='en'>
<head>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<meta name='Description' content='TABSearch'>
	<title>TABSearch</title>
	<meta property='og:title' content='TABSearch' />
	<meta property='og:url' content='https://search.thearcanebrony.net/' />
	<meta property='og:image' content='https://search.thearcanebrony.net/logo.png' />
	<link rel='stylesheet' href='/style.css'>
	<script src='https://code.jquery.com/jquery-3.6.0.min.js'></script>
	<iframe id='navbar' src='/navbar.html' title='Navigation bar'></iframe>
</head>
<main>
	<div id='body'>
<center>
	<input id='q' value='"+Request.Query["q"]+@"'></input>
	<button id='s'>Search</button>
	<script>$('#s').click(x=>{document.location.href = document.location.origin +'/search?q='+$('#q')[0].value})</script><br>
"+
	                                                 $"<p><b>{results.Length} results found</b></p>" + resultsHtml
	                                                 +@"
</center>
</div>
<iframe id='footer' src='/footer.html' title='Footer'/>
</main>
</html>", ContentType = "text/html"};
            }
        }
}